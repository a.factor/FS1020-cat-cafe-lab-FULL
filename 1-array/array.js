let cats = [ 'Tuna', 'Chester', 'Blue' ];

//full array
console.log('full array');
console.log(cats);

//finding one cat
console.log('One cat in the array');
console.log(cats[1]);

// for loop - all cats
console.log('For Loop');
for (let i = 0; i < cats.length; i++) {
	console.log(cats[i]);
}

//for each
console.log('For Each loop');
cats.forEach((cat) => {
	console.log(cat);
});


