//Cat Array
let cats = [
    {
        name: 'Tuna',
        breed: 'Siamese',
        gender: 'Female',
        neutered: true
    },
    {
        name: 'Chester',
        breed: 'Tabby',
        gender: 'Male',
        neutered: false

    },
    {
        name: 'Blue',
        breed: 'Naked',
        gender: 'Female',
        neutered: false

    }
];

const addCat = (name, breed, gender, neutered) => {
    let newCat = { name: name, breed: breed, gender: gender, neutered: neutered };
    cats.push(newCat);
    console.log(cats);
};

addCat('Daisy', 'Tabby', 'Female', true);