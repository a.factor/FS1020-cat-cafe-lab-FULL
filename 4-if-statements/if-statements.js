//Cat Array
let cats = [
	{
		name: 'Tuna',
		breed: 'Siamese',
		gender: 'Female',
		neutered: true
	},
	{
		name: 'Chester',
		breed: 'Tabby',
		gender: 'Male',
		neutered: false
		
	},
	{
		name: 'Blue',
		breed: 'Naked',
		gender: 'Female',
		neutered: false
		
	}
];

const checkCatIsNeutered = () => {
	cats.forEach((cat) => {
		if (cat.neutered === false) {
			console.log(`take ${cat.name} to the vet!`);
		} else {
			console.log(null);
		}
	});
};

checkCatIsNeutered();