let cats = [
	{
		name     : 'Tuna',
		breed      : 'Siamese',
		gender : 'Female',
	},
	{
		name     : 'Chester',
		breed      : 'Tabby',
		gender : 'Male',
	},
	{
		name     : 'Blue',
		breed      : 'Naked',
		gender :'Female', 
	}
];

//forEach Format Example 
// array1.forEach(element => console.log(element));
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach


//1. full array
console.log('full array');
console.log(cats);

//2. finding object of one cat
console.log('One cat in the array');
console.log(cats[1]);

// 3. Find Blue's gender
console.log('blues gender');
console.log(cats[2].gender);

// // //4. full object list - notice the array brackets are missing []
console.log('full object list');
cats.forEach((cat) => {
	console.log(cat);
});


// //5. finding names of all cats
console.log('cat names');
cats.forEach((cat) => {
	console.log(cat.name);
});

